/*************************************************
 * Starts up the game itself
 ************************************************/

import java.util.ArrayList;

public class CaravanGame {
    public static void main (String[] args) {
        Deck deck1 = new Deck("Rafi");
        DiscardPile discardPile1 = new DiscardPile("Rafi");

        for (Card c : deck1.cards()) {
            System.out.println(c.toString());
        }
    }

    // Discard piles only exist during the game, so are more strongly
    // tied to the Caravan class than anything else.
    static class DiscardPile extends Cards {
        public DiscardPile(String user) {
            super(user);
        }
    }
}
