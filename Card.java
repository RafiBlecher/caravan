/*************************************************
 * Provides an implementation of a class to
 * represent a playing card
 ************************************************/

public class Card implements CardConstants {
    //protected ImageIcon cardImage;
    protected Rank rank;
    protected Suit suit;
    protected String deck;

    // Constructs a new generic card
    public Card() {
        this.rank = null;
        this.suit = null;
        this.deck = null;
    }

    // Constructs a specific card
    public Card(Rank rank, Suit suit) {
        this.rank = rank;
        this.suit = suit;
        this.deck = null;
    }

    // Returns the rank
    public Rank getRank() {
        return this.rank;
    }

    public class InvalidSuitException extends Exception {
        public static final long serialVersionUID = 24362463L;
        public InvalidSuitException(String message) {
            super(message);
        }
    }

    public String getSuit() throws InvalidSuitException {
        String suit;

        switch(this.suit) {
            case HEARTS: suit = "Hearts"; break;
            case DIAMONDS: suit = "Diamonds"; break;
            case SPADES: suit = "Spades"; break;
            case CLUBS: suit = "Clubs"; break;
            case NONE: suit = ""; break;
            default: throw new InvalidSuitException("Invalid suit to get name");
        }

        return suit;
    }

    public String getDeck() {
        return this.deck;
    }

    public class InvalidRankException extends Exception {
        public static final long serialVersionUID = 24362462L;
        public InvalidRankException(String message) {
            super(message);
        }
    }

    public String getName() throws InvalidRankException {
        String face;
        switch(this.rank) {
            case ACE: face = "Ace"; break;
            case TWO: face = "Two"; break;
            case THREE: face = "Three"; break;
            case FOUR: face = "Four"; break;
            case FIVE: face = "Five"; break;
            case SIX: face = "Six"; break;
            case SEVEN: face = "Seven"; break;
            case EIGHT: face = "Eight"; break;
            case NINE: face = "Nine"; break;
            case TEN: face = "Ten"; break;
            case JACK: face = "Jack"; break;
            case QUEEN: face = "Queen"; break;
            case KING: face = "King"; break;
            case JOKER: face = "Joker"; break;
            default: throw new InvalidRankException("Invalid rank to create name");
        }
        return face;
    }

    public String toString() {
        String message = null;

        String suit = "";
        String name = "";
        String deck = this.getDeck();

        try {
            name = this.getName();
            suit = this.getSuit();
        } catch (InvalidRankException e) {
            System.err.println("Caught InvalidRankException: " + e.getMessage());
        } catch (InvalidSuitException e) {
            System.err.println("Caught InvalidSuitException: " + e.getMessage());
        }


        if (suit == "") {
            // well, first of all:
            assert this.getRank() == Rank.JOKER;
            
            return name + " from deck " + deck;
        }

        return name + " of " + suit + " from deck " + deck;
    }
}
