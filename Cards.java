/*************************************************
 *
 * Provides an implementation of a class to
 * represent a player's hand
 *
 * This is the deck of cards that a player has
 * once they're actually playing
 *
 ************************************************/

import java.util.ArrayList;

public class Cards {
    ArrayList<Card> cards;
    String user;

    public Cards(String user) {
        this.user = user;
    }

    public int size() {
        return this.cards.size();
    }

    public void add(Card c) {
        this.cards.add(c);
    }

    public void remove(Card c) {
        this.cards.remove(c);
    }

    public void empty() {
        this.cards.clear();
        this.cards = null;
    }

    public boolean isEmpty() {
        return this.size() == 0;
    }

    public void merge(Cards c) {
        this.cards.addAll(c.cards);
    }

    public ArrayList<Card> cards() {
        return this.cards;
    }

    public String getOwner() {
        return this.user;
    }
}
