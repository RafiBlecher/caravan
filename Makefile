JC = javac

JFLAGS = -g

.SUFFIXES: .java .class
.java.class:
	$(JC) $(JFLAGS) $*.java

CLASSES = CaravanGame.java CardConstants.java Card.java Cards.java Deck.java Hand.java

default: classes

classes: $(CLASSES:.java=.class)

clean:
	$(RM) *.class
