import java.util.ArrayList;

public class Hand extends Cards {
    public Hand(String user) {
        super(user);
        this.cards = new ArrayList<Card>();
    }

    // Enters cards into hand if necessary
    public void updateHand(int round) {
        // Round is 0-based. Please, please, please take that into
        // consideration.
        if (round == 0) {
            // select 8 cards randomly from deck, place in hand
        } else if (round >= 3) {
            if (this.size() != 5) {
                // draw a new card from deck into hand
            }
        }
    }

    // Sanity check for hand
    public boolean okayHand(int round) {
        // NOTE this can only be run at the end of a round, or things
        // will break.
        if (round == 0) {
            if (this.size() != 8)
                return false;
        } else if (round <= 2) {
            // players have to place a card in a new caravan
            // without drawing a new card in rounds 1-3
            if (this.size() != 8 - round)
                return false;
        } else {
            if (this.size() != 5)
                return false;
        }

        return true;
    }

    // Discard a card from your hand, put it in the discarded pile
    public void discard(Card c, Cards discarded) {
        assert this.cards.contains(c);

        discarded.add(c);
        this.remove(c);
    }
}
