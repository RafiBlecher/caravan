Caravan
=======

About
-----
I need to learn a new programming language, I'm bored, and the most
GUI-intensive programs that I've written have been websites.

Whiiiich means that it's time to learn Java!

I'm going to create the minigame Caravan from Fallout: New Vegas.

If you see any code that suggests that I have no idea what I'm doing,
please point it out. I've never done something like this before, and
I started learning Java about 2 days before I started coding this.

You can find the rules of Caravan [here](<http://fallout.wikia.com/wiki/Caravan_(game)>)
