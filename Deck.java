import java.util.ArrayList;

public class Deck extends Cards implements CardConstants {
    public Deck(String user) {
        super(user);
        this.retrieve();
        if (this.isEmpty())
            this.createNew(this.cards);
    }

    // Save deck in database. Like I have any idea how to do this.
    /*
    public boolean storeDeck(String username) {
    }
    */

    // retrieve deck from database
    public void retrieve() {
        // TODO get from DB, using this.user
        this.cards = new ArrayList<Card>();
    }

    // Fills an empty deck with a standard deck of cards
    private void createNew(ArrayList<Card> deck) {
        assert deck.size() == 0;

        for (Suit suit : Suit.values())
            for (Rank rank : Rank.values())
                if (rank != Rank.JOKER && suit != Suit.NONE)
                    deck.add(new Card(rank, suit));
        deck.add(new Card(Rank.JOKER, Suit.NONE));
        deck.add(new Card(Rank.JOKER, Suit.NONE));
    }

    // Move all cards back to deck. Designed to be run when a game is finished
    public void restore(Cards hand, Cards discarded) {
        this.merge(hand);
        this.merge(discarded);

        hand.empty();
        discarded.empty();
    }
}
